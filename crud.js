//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/


const http = require('http');

const port = 8000;

let products = [
    {
        "name": "Gaming Monitor",
        "description": "Asus TUF Gaming VG247Q1A 23.8\" Gaming Monitor [165Hz]",
        "price": 10495,
        "stocks": 5,
    },
    {
        "name": "Mobile Phone",
        "description": "Xiaomi Redmi 10 Internal 128GB+6GB RAM 6.5\" FHD+ 90Hz",
        "price": 8498,
        "stocks": 10,
    },
    {
        "name": "Gaming Console",
        "description": "Nintendo Switch Mario Red & Blue Edition NSW Console",
        "price": 18909,
        "stocks": 4,
    }
];

const server = http.createServer(function(req,res){

	let url = req.url;
    let method = req.method;
    let response;
	
    if(url == '/'){
        response = 'Welcome to Ordering System';
    }else if(url == '/dashboard'){
        response = 'Welcome to your User\'s Dashboard!';
    }else if(url == '/products'){
        response = 'Here\'s our products available';
    }else if(url == '/addProduct' && method == 'POST'){
        response = 'Added a product to our resources';

		let request_body = '';
        req.on('data', function(data){
			request_body += data
		})

        req.on('end', function() {

            request_body = JSON.parse(request_body);

			let product = {
                "name": request_body.name,
                "description": request_body.description,
                "price": request_body.price,
                "stocks": request_body.stocks
			}

            products.push(product)
			
			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(products))
			res.end(response);

		})
    }else if(url == '/updateProduct' && method == 'PUT'){
        response = 'Update a product to our resources';
    }else if(url == '/archiveProduct' && method == 'DELETE'){
        response = 'Archive products to our resources';
    }


    if(url != '/addProduct'){
        res.writeHead(200,{'Content-Type':'text/html'})
        res.end(response);
    }

})

server.listen(port);

console.log(`Server is running on localhost:${port}`);