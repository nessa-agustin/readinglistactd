console.log('Hello World');

/* 1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section) */

function getSection(score){

    let section;

    if(score < 81){
        section = "Ruby";
    }else if(score < 121){
        section = "Opal";
    }else if(score < 161){
        section = "Sapphire";
    }else if(score > 160){
        section = "Diamond";
    }

    return `Your score is ${score}. You will proceed to Grade 10 ${section}.`

}

console.log(getSection(74));
console.log(getSection(85));
console.log(getSection(135));
console.log(getSection(160));
console.log(getSection(171));

/* 
2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/


function findLongestString(str){

    const arr = str.split(' ');

    let arrMap =  arr.map(x => {
        return x.length;
    })

    let arrMapSort = arr.map(x => {
        return x.length;
    }).sort((a,b) => { return b - a});

    let index = arrMap.indexOf(arrMapSort[0])

    console.log(arr[index])


}

findLongestString("Web Development Tutorial");

/* 
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'
*/

function findChar(str){

    const arr = str.split('');

    const filterArr = arr.filter(c => {
        
        let cMap = arr.filter(m => {return m == c});
        let cnt = cMap.length;

        return cnt == 1; 
    })

    // console.log(filterArr[0]);
    return filterArr[0];

}

findChar('abacddbec')
findChar('rtyjftytyu')